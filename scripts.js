// Arrays it is a collection or a list.
var potions = []; //empty array
// Add an item in array
potions[2] = "Yellow herbs";
potions[1] = "Red Potion";
potions[0] = "Green Potion";
potions[3] = "Yggdrasil berries";
// console.log(potions.length);
// console.log(potions);
var skills = ["Rampage","Wind walk","Hook","Bash","Double Strafe"];
// console.log(skills[skills.length - 3]);
// pop,push,shif and unshift
// console.log(skills);
// skills.pop();
// console.log(skills);
// skills.push("New Item");
// console.log(skills);
// skills.shift();
// console.log(skills);
// skills.unshift("Total Darkness!");
// console.log(skills);

// Multi Array or Nested Arrays
var player = [ ["Wizard","Archer","Assassin","Knight",15,78,12.67],
["Dokibi","Dark Knight","Gellopi","Poring",["Evil poring","Dragonoid"] ] ]

// console.log(player.length);
// console.log(player[0][2]);
// console.log(player[1][4][1]);

// Merging Arrays
var newPlayer = player.concat([["Dark Lord","Orc Lord","Baphomet","Maya","Orc Hero","Drake"]]);
console.log(newPlayer);
// SLICING AN ARRAY
var heroes = ["Drake","Zoro","Luffy","Nami","Doflamingo"]
// console.log(heroes.slice(0,3));
console.log(newPlayer[2].slice(0,4));

var playerLevel = 16;
var skillPoints = 10;
// Control Structures
if (playerLevel > 20){
  skillPoints += 10;
  console.log("You have leveled up!");
}else if (playerLevel < 15) {
  console.log("Need to find monsters to kill!");
}
else{
  skillPoints = 0;
  console.log("You are still in level " + playerLevel );
}
